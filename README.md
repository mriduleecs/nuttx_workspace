# README #

## What is this repository for? ###

This Repository is my work on NuttX RTOS.

### How do I get set up? ###

#####Install OpenOCD#####

apt-get install automake bison build-essential flex gcc-arm-none-eabi gperf git libncurses5-dev libtool libusb-dev libusb-1.0.0-dev

mkdir ~/NuttX_stm32f4disco

cd ~/NuttX_stm32f4disco 

git clone http://repo.or.cz/r/openocd.git 

cd ~/NuttX_stm32f4disco/openocd 

./bootstrap 

./configure --enable-internal-jimtcl --enable-maintainer-mode --disable-werror --disable-shared --enable-stlink --enable-jlink --enable-rlink --enable-vslink --enable-ti-icdi --enable-remote-bitbang 

make 

sudo make install

#####Clone NuttX, Tools, Apps#####

cd ..

git clone https://bitbucket.org/nuttx/nuttx 

git clone https://bitbucket.org/nuttx/apps

git clone https://bitbucket.org/nuttx/tools

cd tools/kconfig-frontends/ 

./configure 

make 

sudo make install 

sudo ldconfig 

cd ../.. 

cd nuttx/tools/ 

./configure.sh stm32f4discovery/usbnsh 

cd .. 

make menuconfig 

make 

ls -l nuttx.bin

#####Install STLINK#####

git clone https://github.com/texane/stlink stlink.git

cd stlink

make

sudo cp build/Debug/st-* /usr/local/bin

sudo cp etc/udev/rules.d/49-stlinkv* /etc/udev/rules.d/

sudo restart udev

#####Flashing NuttX in the STM32F4discovery Board#####

cd openocd/contrib/ 

sudo cp 60-openocd.rules /etc/udev/rules.d/ 

sudo udevadm trigger 

cd ../.. 

cd nuttx 

openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg 

openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg -c init -c "reset halt" -c "flash write_image erase nuttx.bin 0x08000000"
