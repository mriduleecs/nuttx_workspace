/**************************************************************************//**
 * @file     crc.c
 * @version  V3.00
 * $Revision: 2 $
 * $Date: 14/06/06 11:22a $
 * @brief    M451 series CRC driver source file
 *
 * @note
 * Copyright (C) 2013 Nuvoton Technology Corp. All rights reserved.
*****************************************************************************/
#include "M451Series.h"


/** @addtogroup M451_Device_Driver M451 Device Driver
  @{
*/

/** @addtogroup M451_CRC_Driver CRC Driver
  @{
*/

/** @addtogroup M451_CRC_EXPORTED_FUNCTIONS CRC Exported Functions
  @{
*/

/**
 * @brief       Open CRC to start
 *
 * @param[in]   u32Mode         CRC polynomial mode. It consists of: CRC_CCITT, CRC_8, CRC_16 or CRC_32.
 * @param[in]   u32Attribute    Parameter attribute. It consists of: CRC_CHECKSUM_COM, CRC_CHECKSUM_RVS, CRC_WDATA_COM and CRC_WDATA_RVS.
 * @param[in]   u32Seed         Seed value.
 * @param[in]   u32DataLen      CPU write data length. It consists of: CRC_CPU_WDATA_8, CRC_CPU_WDATA_16 or CRC_CPU_WDATA_32.
 *
 * @return      None
 *
 * @details     This function will enable the CRC controller by specify CRC operation mode, attribute, initial seed and write data length. \n
 *              After that, user can start to perform CRC calculate by calling CRC_WRITE_DATA macro or CRC_DAT register directly.
 */
void CRC_Open(uint32_t u32Mode, uint32_t u32Attribute, uint32_t u32Seed, uint32_t u32DataLen)
{
    /* Enable CRC channel clock */
    CLK->AHBCLK |= CLK_AHBCLK_CRCCKEN_Msk;

    CRC->SEED = u32Seed;
    CRC->CTL = u32Mode | u32Attribute | u32DataLen | CRC_CTL_CRCEN_Msk;

    /* Setting CRCRST bit will reload the initial seed value(CRC_SEED register) to CRC controller */
    CRC->CTL |= CRC_CTL_CRCRST_Msk;
}

/**
 * @brief       Get CRC Checksum
 *
 * @param[in]   None
 *
 * @return      Checksum Result
 *
 * @details     This macro gets the CRC checksum result by current CRC polynomial mode.
 */
uint32_t CRC_GetChecksum(void)
{
    switch(CRC->CTL & CRC_CTL_CRCMODE_Msk)
    {
    case CRC_CCITT:
    case CRC_16:
        return (CRC->CHECKSUM & 0xFFFF);

    case CRC_32:
        return (CRC->CHECKSUM);

    case CRC_8:
        return (CRC->CHECKSUM & 0xFF);

    default:
        return 0;
    }
}

/*@}*/ /* end of group M451_CRC_EXPORTED_FUNCTIONS */

/*@}*/ /* end of group M451_CRC_Driver */

/*@}*/ /* end of group M451_Device_Driver */

/*** (C) COPYRIGHT 2013 Nuvoton Technology Corp. ***/
